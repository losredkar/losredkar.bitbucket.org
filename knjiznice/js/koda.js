
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var teeza=0;
var viisina=0;
var itmm=0;
var stikalo =0;
var itmbarva="#336699"
/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke1() {
  ime = "Rebel";
  priimek = "Wilson";
  datumRojstva="1980-03-02";
  var tezaa = 110;
  var visinaa = 159;
    

    $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        var podatki = {
  			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
        // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
  		    "ctx/language": "en",
  		    "ctx/territory": "SI",
  		    "vital_signs/height_length/any_event/body_height_length": visinaa,
  		    "vital_signs/body_weight/any_event/body_weight": tezaa,
		
		    };
		    var parametriZahteve = {
  		    ehrId: ehrId,
  		    templateId: 'Vital Signs',
  		    format: 'FLAT'
		    };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo3").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR za Rebel Wilson '" +
                ehrId + "'.</span>");
              $("#rebel").attr("value", ehrId);
              $("#preberiEHRid").val(ehrId);
               $("#meritveVitalnihZnakovEHRid").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo3").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
         $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki),
            headers: {
              "Authorization": getAuthorization()
            },
            success: function (res) {
              $("#dodajMeritveVitalnihZnakovSporocilo2").html(
                "<span class='obvestilo label label-success fade-in'>" + "Uspešno vnešena višina ("+visinaa+" cm) in teža ("+tezaa+" kg)"+" za Rebel Wilson</span>");
            },
            error: function(err) {
            	$("#dodajMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                JSON.parse(err.responseText).userMessage + "'!");
            }
      		});
        
      }
		});
		
}

function generirajPodatke2() {
    var ehrId
    var ime;
    var priimek;
    var datumRojstva;
    var tezaa = 65;
    var visinaa = 168;
    
      ime = "Kendrick"
      priimek = "Lamar"
      datumRojstva="1987-06-17"


    $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        var podatki = {
  			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
        // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
  		    "ctx/language": "en",
  		    "ctx/territory": "SI",
  		    "vital_signs/height_length/any_event/body_height_length": visinaa,
  		    "vital_signs/body_weight/any_event/body_weight": tezaa,
		
		    };
		    var parametriZahteve = {
  		    ehrId: ehrId,
  		    templateId: 'Vital Signs',
  		    format: 'FLAT'
		    };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo2").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR za Kendrick Lamar '" +
                ehrId + "'.</span>");
              $("#kendrick").attr("value", ehrId);
              $("#preberiEHRid").val(ehrId);
              $("#meritveVitalnihZnakovEHRid").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo2").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
         $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki),
            headers: {
              "Authorization": getAuthorization()
            },
            success: function (res) {
              $("#dodajMeritveVitalnihZnakovSporocilo3").html(
                "<span class='obvestilo label label-success fade-in'>" + "Uspešno vnešena višina ("+visinaa+" cm) in teža ("+tezaa+" kg)"+" za Kendrick Lamar </span>");
            },
            error: function(err) {
            	$("#dodajMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                JSON.parse(err.responseText).userMessage + "'!");
            }
      		});
        
        
      }
		});
  
 

}
function generirajPodatke3() {
    var ehrId
    var ime;
    var priimek;
    var datumRojstva;
    var tezaa;
    var visinaa;
    
      ime = "Spidi";
      priimek = "Gonzalez";
      datumRojstva="1953-08-29";
      tezaa = 1.5;
      visinaa=30;


    $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        var podatki = {
  			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
        // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
  		    "ctx/language": "en",
  		    "ctx/territory": "SI",
  		    "vital_signs/height_length/any_event/body_height_length": visinaa,
  		    "vital_signs/body_weight/any_event/body_weight": tezaa,
		
		    };
		    var parametriZahteve = {
  		    ehrId: ehrId,
  		    templateId: 'Vital Signs',
  		    format: 'FLAT'
		    };
        
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo4").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR za Spidi Gonzalez '" +
                ehrId + "'.</span>");
               $("#spidi").attr("value", ehrId);
               $("#preberiEHRid").val(ehrId);
               $("#meritveVitalnihZnakovEHRid").val(ehrId);
              
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo4").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
        $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki),
            headers: {
              "Authorization": getAuthorization()
            },
            success: function (res) {
              $("#dodajMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-success fade-in'>" + "Uspešno vnešena višina ("+visinaa+" cm) in teža ("+tezaa+" kg)"+" za Spidi Gonzalez </span>");
            },
            error: function(err) {
            	$("#dodajMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                JSON.parse(err.responseText).userMessage + "'!");
            }
      		});
      }
        	
      
		});
		
		
	
		
  


}






function kreirajEHRzaBolnika() {
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "' ("+ime+" "+priimek+" "+datumRojstva+").</span>");
              $("#preberiEHRid").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth + 
          "'.</span>");
  		},
  		error: function(err) {
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
  		}
		});
	}
}


/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {
	var ehrId = $("#dodajVitalnoEHR").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();


	if (!ehrId || ehrId.trim().length == 0 || telesnaTeza.trim().length==0 || telesnaVisina.trim().length==0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT'
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        teeza=telesnaTeza;
        viisina=telesnaVisina;
        $("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-success fade-in'>" + "Uspešno vnešena višina ("+telesnaVisina+" cm) in teža ("+telesnaTeza+" kg)"+" za EHR ID: "+ehrId+" </span>");
      },
      error: function(err) {
      	$("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
}


/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura in telesna teža).
 */
function preberiMeritveVitalnihZnakov() {
  var telesna_teza;
  var telesna_visina;
  var itm;
  
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var tip = $("#preberiTipZaVitalneZnake").val();

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
	   var teza2;
     var visina2;
     var itm2;
  
  					$.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
                      teza2 = res[0].weight;
                      teeza = res[0].weight;
  				    },
  				       
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				
  				
  					$.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/" + "height",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
  				           visina2=res[0].height;
  				           viisina = res[0].height;
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				
	      	$.ajax({
      			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
      			type: 'GET',
      			headers:  {"Authorization": getAuthorization()
      			},
      			
      	    	success: function (res) {
      	    		//console.log(data);
      				var party = res.party;
      				var visinaM=visina2/100;
      				itm2 =((teza2)/((visinaM*visinaM)));
      			  var rez =	itm2.toFixed( [1] );
      			  itmm=rez;
      			  
      			  if(rez<18.5){
      			    itmbarva="#0099ff"
      			    $("#rezultatMeritveVitalnihZnakov").html("<br/><span>ITM za osebo " + party.firstNames +" "+party.lastNames+" "+"znaša<b> "+rez+ "</b>.<p style=color:#0099ff;> Stanje nakazuje na <b>zmanjšano telesno težo<b></p>");
      			  }
      			  else if(rez>=18.5 && rez< 25.0){
      			    itmbarva="#2eb82e";
      			    $("#rezultatMeritveVitalnihZnakov").html("<br/><span>ITM za osebo " + party.firstNames +" "+party.lastNames+" "+"znaša <b>"+rez+ "</b>. <p style=color:#2eb82e;>Stanje nakazuje na <b> normalno telesno težo<b></p>");
      			  }
      			  else if(rez>=25 && rez< 30){
      			    itmbarva="#cc7a00";
      			    $("#rezultatMeritveVitalnihZnakov").html("<br/><span>ITM za osebo " + party.firstNames +" "+party.lastNames+" "+"znaša<b> "+rez+ "</b>.<p style=color:#cc7a00;> Stanje nakazuje na <b>rahlo povišano telesno težo</b></p>");
      			  }
      				else if (rez>=30){
      				  itmbarva="#990000";
      				  $("#rezultatMeritveVitalnihZnakov").html("<br/><span>ITM za osebo " + party.firstNames +" "+party.lastNames+" "+"znaša<b> "+rez+ "</b>.<p style=color:#990000;> Stanje nakazuje na <b>debelost</b></p>");
      				}
      				else{
      			  	$("#rezultatMeritveVitalnihZnakov").html("Ni vnešenih podatkov o višini in teži!");
      				}
      				
      				//console.log(party);
      				//console.log(party.partyAdditionalInfo);
      				itm2 =Math.round(teza2)/((visina2*visina2));
      				
      	
      				
      				
      				
      			},
      			error: function(err) {
      				$("#preberiSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
      				console.log(JSON.parse(err.responseText).userMessage);
      			}
		});
	    
	    
	    
	    
	   
	    stikalo = 1;
	     document.getElementById("gumbzagraf").disabled = false;
	     // itm=(telesna_teza)/((telesna_visina*telesna_visina));
	
	}
	
//	console.log("predzadnja "+telesna_teza);
//	console.log("predzadnja "+telesna_visina);
//	console.log("zadnja");

  
}






// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
$(document).ready(function() {


$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
	});


	$('#preberiEhrIdZaVitalneZnake').change(function() {
	
			$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});







});

function izrisiGraf(){
  
  if(itmm!=0 && viisina !=0 && teeza!=0 && stikalo!=0){
  
      var ctx = document.getElementById('myChart').getContext('2d');
    
      var chart = new Chart(ctx, {
        type: 'bar',
        
        data: {
            labels: ['Telesna teža [kg]','ITM','Visina [cm]'],
            datasets: [{
                label: 'Grafični prikaz vnesenih podatkov',
                backgroundColor : [
              "#00aaff", itmbarva, "#006699"],
                borderColor: "black",
                data: [teeza,itmm,viisina]
            }]
        },
        options: {}
    });
    stikalo=0;
    $("#preberiMeritveVitalnihZnakovSporocilo").html("");
    //document.getElementById("gumbzagraf").disabled = true;
  }
  else{
    $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>← Najprej izračunajte ITM za izbran EHR-ID!");
  }
  
}